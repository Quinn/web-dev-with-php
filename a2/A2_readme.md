> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Assignment 2 Requirements:
#### Two Parts:
1. Finishthe following tutorial:(DO NOT INSTALL MYSQL! IT IS INCLUDED WITH AMPPS!)https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html
2. Questions A2 (Ch5,6)
3. Bitbucket repos link
  -----------------------------------------------
### Assignment 2 README.md file should include:
  - Screenshot of Assessment
  - Bitbucket repo link 
  -----------------------------------------------
## Assignment Screenshots: 
*Screenshot of running http://localhost:9999/hello*
![A2 a](img/a.png)

*Screenshot of running http://localhost:9999/hello/index.html*
![A2 b](img/b.png)

*Screenshot of running http://localhost:9999/hello/sayhello*
![A2 c](img/c.png)

*Screenshot of running http://localhost:9999/hello/querybook.html*
![A2 d](img/d.png)

*Screenshot of running http://localhost:9999/hello/sayhi*
![A2 e](img/e.png)

*Screenshot of running http://localhost:9999/hello/querybook.html/ (select item) *
![A2 f](img/f.png)

*query results*
![A2 g](img/g.png)









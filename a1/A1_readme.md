> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Assignment 1 Requirements:
#### Three Parts:
1. Distributed version control with Git and Bitbucket
2. Java/JDK/Servlet Development Installation
3. Questions A1 (Ch1-4)
4. Bitbucket repos link
  -----------------------------------------------
### Assignment 1 README.md file should include:
  - git commands with short descriptions
  - Screenshot of running Java Hello
  - Screenshot of running http://localhost:9999/lis4368/
  - Bitbucket repo link
  -----------------------------------------------

# Git commands w/short descriptions:
1. git init - create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories

## Assignment Screenshots: 
*Screenshot of running Java Hello*
![A1 jdk](img/jdk_install.png)

*Screenshot of running http://localhost:9999/*
![A1 tomcat](img/tomcat.png)

*Screenshot of running http://localhost:9999/lis4368/a1*
![A1 a1](img/a.png)

*Screenshot of running http://localhost:9999/ homepage*
![A1 home](img/b.png)

 
[A1 Bitbucket repos](https://bitbucket.org/qn18/lis4368/src/master/ "Bitbucket repos")








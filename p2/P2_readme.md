> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Project 2 Requirements:
#### Six Parts:
1. Review subdirectories and files, especially META-INF and WEB-INF.
2. Providing create, read, updateand delete(CRUD) functionality.
3. Edit and compile CustomerServlet.java 
4. Screenshots of Assignment
5. Questions P2 (Ch16-17) 
6. Bitbucket repos link
  -----------------------------------------------
### Project 2 README.md file should include:
  - Screenshot of project (CRUD)
  - Screenshot of mysql
  - Bitbucket repo link
  -----------------------------------------------
## Assignment Screenshots: 
*Valid User Form Entry (customerform.jsp)---------PassedValidation (thanks.jsp)*
![P2 a](img/a.png)

*Display Data (customers.jsp)*
![P2 b](img/b.png)

*Modify Form (modify.jsp)*
![P2 c](img/c.jpg)

*Modified Data (customers.jsp)*
![P2 d](img/d.png)

*Delete Warning (customers.jsp)*
![P2 e](img/e.png)

*Associated Database Changes (Select, Insert, Update, Delete) *
![P2 f](img/f.png)









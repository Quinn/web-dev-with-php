> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Assigmnet 4 Requirements:
#### Five Parts:
1. Edit and compile CustomerServlet.java and Customer.java
2. Screenshots of Assignment
    - A4 Failed Validation
    - A4 Passed Validagtion
3. Questions A4 (Ch11-12) 
4. Bitbucket repos link
5. Assignment links http://localhost:9999/lis4368/customerform.jsp?assign_num=a4
  -----------------------------------------------
### README.md file should include:
  - Screenshot of A4 Failed Validation
  - Screenshot of A4 Passed Validation
  -----------------------------------------------
## Assignment Screenshots: 
*Screenshot of A4 Failed Validation
![A4 fail](img/a.png)

*Screenshot of A4 Passed Validation
![A4 pass](img/b.png)

[A4 link to local host](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4 "link")









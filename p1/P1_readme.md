> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Project 1 Requirements:
#### Three Parts:
1. Review subdirectories and files, especially META-INF and WEB-INF.
2. Each assignment *must* have its own globalsubdirectory.
3. Open index.jspand review code
4. Questions P1 (Ch9-10) 
5. Bitbucket repos link
  -----------------------------------------------
### Project 1 README.md file should include:
  - Screenshot of Customer in localhost
  - Screenshot of running http://localhost:9999/lis4368/
  - Bitbucket repo link
  -----------------------------------------------

## Assignment Screenshots: 
*Screenshot of running http://localhost:9999/lis4368/index.jsp*
![P1 fail](img/a.png)

*Screenshot of running http://localhost:9999/lis4368/p1/index.jsp*
![P1 pass](img/b.png)


 
[P1 Bitbucket repos](https://bitbucket.org/qn18/lis4368/src/master/ "Bitbucket repos")








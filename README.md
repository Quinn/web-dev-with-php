> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368

## Quinn Nguyen

### MAIN README:

*Course Work Links:*

1. [A1 README.md](a1/A1_readme.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command description

2. [A2 README.md](a2/A2_readme.md "My A2 README.md file")
    - Tutorial Tomcat_howto
    - Develop and Deploy a WebApp
    - Compiling the Servlet
    - Invoke the Servlet
    - Write a Database Servlet
    - Setup a Database on MySQL
    - Write the Server-side Database Query Servlet
    - Screenshots of Assessment

3. [A3 README.md](a3/A3_readme.md "My A3 README.md file")
    - Entity Relationship Diagram 
    - Having data (min. 10 records per table).
    - Create Docs folder: a3.mwb and a3.sql
    - SQL Statement
    
4. [A4 README.md](a4/A4_readme.md "My A4 README.md file")
    - Edit and compile CustomerServlet.java and Customer.java
    - Screenshots of Assignment (Failed and Passed Validation)
    - Questions A4 (Ch11-12) 
    - Canvas Links: Bitbucket repo

5. [A5 README.md](a5/A5_readme.md "My A5 README.md file")
    - Edit A5 webpage
    - Show data was entered into database that was created in A3
    - Screenshots of Assignment (Associated databasae entry)

6. [P1 README.md](p1/P1_readme.md "My P1 README.md file")
    - Review subdirectories and files, especially META-INF and WEB-INF.
    - Each assignment *must* have its own globalsubdirectory.
    - Open index.jspand review code
    - Questions P1 (Ch9-10) 

7. [P2 README.md](p2/P2_readme.md "My P2 README.md file")
    - Review subdirectories and files, especially META-INF and WEB-INF.
    - Providing create, read, updateand delete(CRUD) functionality.
    - Edit and compile CustomerServlet.java 
    - Questions P2 (Ch16-17) 




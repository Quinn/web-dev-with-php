> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Assignment 3 Requirements:
1. ERD and SQL statement
2. Having data (min. 10 records per table)
3. Questions A3 (Ch7-8)
4. Bitbucket repos link include:
  - docs folder: a3.mwb and a3.sql
  - img folder: Erd pic
  -----------------------------------------------
### Assignment 3 README.md file should include:
  - Screenshot of ERD
  - Docs folder: a3.mwb and a3.sql
  -----------------------------------------------
### Assignment Screenshots: 
*Screenshot of ERD*
![A3 erd](img/a.png)

*Screenshot of SQL statement*
![A3 sql](img/b.png)

### Assignment Links: 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3s.sql "A3 SQL Script")
 









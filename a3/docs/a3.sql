-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema qn18
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `qn18` ;

-- -----------------------------------------------------
-- Schema qn18
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `qn18` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `qn18` ;

-- -----------------------------------------------------
-- Table `qn18`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qn18`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `qn18`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_vtd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `qn18`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qn18`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `qn18`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `qn18`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qn18`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `qn18`.`pet` (
  `pet_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `qn18`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `qn18`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `qn18`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `qn18`;
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstorea', '123 Streeta', 'Citya', 'FL', 32311, 9876543210, 'hfdj@email.com', 'fdhfj.com', 70000, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstoreb', '123 Streetb', 'Cityb', 'FL', 32312, 9876543211, 'hfddj@email.com', 'fdhfjs.com', 70001, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstorec', '123 Streetc', 'Cityc', 'FL', 32314, 9876543212, 'hfsdj@email.com', 'fdshfj.com', 70002, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstored', '123 Streetd', 'Cityd', 'FL', 32313, 9876543213, 'hafdj@email.com', 'fddhfj.com', 70003, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstoree', '123 Streete', 'Citye', 'FL', 32315, 9876543214, 'hfedj@email.com', 'fdchfj.com', 70004, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstoref', '123 Streetf', 'Cityf', 'FL', 32316, 9876543215, 'hfbdj@email.com', 'fgdhfj.com', 70005, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstoreg', '123 Streetg', 'Cityg', 'FL', 32317, 9876543216, 'hfdcj@email.com', 'fdhyfj.com', 70006, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstoreh', '123 Streeth', 'Cityh', 'FL', 32318, 9876543217, 'hfdjh@email.com', 'fdhfuj.com', 70007, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstorei', '123 Streeti', 'Cityi', 'FL', 32319, 9876543218, 'hfkdj@email.com', 'fdhfji.com', 70008, NULL);
INSERT INTO `qn18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'petstorey', '123 Streety', 'Cityy', 'FL', 32310, 9876543219, 'hfdlj@email.com', 'fdhofj.com', 70009, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `qn18`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `qn18`;
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamea', 'lnamea', '345 Streeta', 'Citya', 'FL', 32310, 9786543210, 'fgh@email.com', 7000, 2000, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnameb', 'lnameb', '345 Streetd', 'Cityd', 'FL', 32312, 9786543211, 'fghd@email.com', 7001, 2002, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamec', 'lnamed', '345 Streete', 'Cityh', 'FL', 32311, 9786543212, 'fgfdf@email.com', 7002, 2010, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamed', 'lnamec', '345 Streetc', 'Cityk', 'FL', 32314, 9786543214, 'fghfda@email.com', 7003, 2001, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamee', 'lnamee', '345 Streetb', 'Cityo', 'FL', 32315, 9786543213, 'dfdfa@email.com', 7004, 2003, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamef', 'lnameff', '345 Streete', 'Cityp', 'FL', 32316, 9786543215, 'fvcgh@email.com', 7005, 2004, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnameg', 'lnameg', '345 Streetf', 'Cityl', 'FL', 32317, 9786543216, 'fgerh@email.com', 7006, 2005, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnameh', 'lnamegg', '345 Streetg', 'Cityu', 'FL', 32318, 9786543217, 'fghy@email.com', 7007, 2006, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamei', 'lnamei', '345 Streeth', 'City', 'FL', 32319, 9786543218, 'fgdh@email.com', 7008, 2007, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamey', 'lnameu', '345 Streeti', 'Cityt', 'FL', 32313, 9786543219, 'fgnhmh@email.com', 7009, 2000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `qn18`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `qn18`;
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Chihuahua', 'm', 300, 400, 3, 'white', '2015-01-10', 'y', 'y', NULL, 1, 1);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Corgi', 'f', 400, 500, 4, 'yellow', '2015-01-11', 'y', 'n', NULL, 3, 2);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Husky', 'm', 500, 600, 5, 'gray', '2015-01-12', 'y', 'y', NULL, 2, 3);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Alaska', 'f', 600, 800, 6, 'gray', '2015-01-13', 'y', 'n', NULL, 5, 4);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Shinba', 'm', 700, 800, 3, 'yellow', '2015-01-14', 'y', 'y', NULL, 6, 5);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Dashshund', 'f', 800, 900, 4, 'black', '2015-01-15', 'y', 'n', NULL, 4, 6);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Pomeranian', 'm', 900, 1000, 5, 'white', '2015-01-16', 'y', 'y', NULL, 1, 1);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Yorkie', 'f', 1000, 1100, 6, 'brown', '2015-01-17', 'y', 'n', NULL, 2, 2);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'German Sherpherd', 'm', 1100, 1200, 3, 'black', '2015-01-18', 'y', 'y', NULL, 3, 3);
INSERT INTO `qn18`.`pet` (`pet_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`, `pst_id`, `cus_id`) VALUES (DEFAULT, 'Golder Retriever', 'f', 850, 900, 4, 'yellow', '2015-01-19', 'y', 'n', NULL, 4, 4);

COMMIT;


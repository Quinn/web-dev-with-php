/* A3 SQL Statement */ 

/* List only the pet store IDs, full address, and phone number for all of the pet stores*/ 
select pst_id,pst_street,pst_city,pst_state,pst_zip, pst_phone from petstore;
/* Display the pet store name, along with the number of pets each pet store has */ 
select pst_name, count(pet_id) as `number of pets`
from petstore
natural join pet
group by pst_id;
/* List each pet store ID, along with all of the customer first, last names and balances associated with each pet store. */ 
select pst_id, cus_fname, cus_lname, cus_balance
from petstore
natural join pet
natural join customer;
/* Update the customer last name to 'Valens' for Customer #2. */ 
update customer
set cus_lname='Valens'
where cus_id=2;
/* Remove Pet #4 */ 
delete from pet where pet_id=4;
/* Add two more customers */ 
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamegi', 'lnadmei', '345 Streetyh', 'City', 'FL', 32319, 9786554218, 'fgdhd@email.com', 7018, 2107, NULL);
INSERT INTO `qn18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'fnamdey', 'lnagmeu', '345 Streetir', 'Cityt', 'FL', 32313, 9786653219, 'fgnhmgh@email.com', 7029, 2400, NULL);
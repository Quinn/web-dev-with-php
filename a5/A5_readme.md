> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Quinn Nguyen

### Assigmnet 5 Requirements:
#### Five Parts:
1. Edit and compile CustomerServlet.java and data pack
2. Screenshots of Assignment
    - A5 Failed Validation
    - A5 Passed Validagtion
    - Associated Database Entry
3. Questions A5 (Ch13-15) 
4. Bitbucket repos link
5. Assignment links http://localhost:9999/lis4368/customerform.jsp?assign_num=a5
  -----------------------------------------------
### README.md file should include:
  - Screenshot of A5 Failed Validation
  - Screenshot of A5 Passed Validation
  -----------------------------------------------
## Assignment Screenshots: 
*Screenshot of Valid User Form Entry
![A5 user](img/a.png)

*Screenshot of A5 Passed Validation
![A5 pass](img/b.png)

*Screenshot of Associated Database Entry
![A5 data](img/c.png)

[A5 link to local host](http://localhost:9999/lis4368/customerform.jsp?assign_num=a5 "link")








